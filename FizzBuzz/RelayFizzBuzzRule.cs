using System;

namespace FizzBuzz
{
    /// <summary>
    /// Helper class to simplify creating rules without the extra overhead.
    /// </summary>
    public class RelayFizzBuzzRule : IFizzBuzzRule
    {
        private readonly Func<int, string> _rule;

        public RelayFizzBuzzRule(Func<int, string> rule)
        {
            _rule = rule;
        }

        public string GetStringForInt(int number)
        {
            return _rule(number);
        }
    }
}