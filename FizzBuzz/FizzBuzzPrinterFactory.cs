using System.Collections.Generic;

namespace FizzBuzz
{
    public static class FizzBuzzPrinterFactory
    {
        /// <summary>
        /// Build the standard "fizz" "buzz" printer
        /// prints fizz for numbers divisible by 3, buzz for divisible by 5, and just the number for other standardRules.
        /// </summary>
        /// <returns></returns>
        public static FizzBuzzPrinter BuildFizzBuzzPrinter()
        {
            var fizzBuzzRules = new List<IFizzBuzzRule>
                {
                    FizzBuzzRuleFactory.BuildDivisibleRule(3,"fizz"),
                    FizzBuzzRuleFactory.BuildDivisibleRule(5,"buzz")
                };
            return new FizzBuzzPrinter(fizzBuzzRules, new NumberToStringRule(), new DefaultConsoleWriteLine());
        }

        /// <summary>
        /// Build a custom fizzbuzz printer.
        /// </summary>
        /// <param name="standardRules">the set of fizzbuzz standardRules to print</param>
        /// <param name="defaultRule">the rule to use if none of the standardRules produce a string</param>
        /// <returns></returns>
        public static FizzBuzzPrinter CustomFizzBuzzPrinter(IEnumerable<IFizzBuzzRule> standardRules, IFizzBuzzRule defaultRule)
        {
            return new FizzBuzzPrinter(standardRules, defaultRule, new DefaultConsoleWriteLine());
        }
    }
}