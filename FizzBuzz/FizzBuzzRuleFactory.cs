namespace FizzBuzz
{
    /// <summary>
    /// Factory for common types of fizzbuzz rules.
    /// </summary>
    public static class FizzBuzzRuleFactory
    {
        /// <summary>
        /// Build a new rule that will include "ruleString" for each number if it is divisible by the divisor.
        /// </summary>
        /// <param name="divisor">the divisor to use</param>
        /// <param name="ruleString">the string to append</param>
        /// <returns></returns>
        public static IFizzBuzzRule BuildDivisibleRule(int divisor, string ruleString)
        {
            return new RelayFizzBuzzRule(number =>
                {
                    if (number % divisor == 0)
                    {
                        return ruleString;
                    }
                    return string.Empty;
                });
        }
    }
}