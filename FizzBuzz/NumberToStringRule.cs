namespace FizzBuzz
{
    /// <summary>
    /// convenience implementation of the ToString rule 
    /// </summary>
    public class NumberToStringRule : IFizzBuzzRule
    {
        public string GetStringForInt(int number)
        {
            return number.ToString();
        }
    }
}