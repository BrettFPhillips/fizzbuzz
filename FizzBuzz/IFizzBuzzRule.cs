namespace FizzBuzz
{
    public interface IFizzBuzzRule
    {
        /// <summary>
        /// return a string if the rule applies to number, otherwise returns string.Empty
        /// </summary>
        string GetStringForInt(int number);
    }
}