﻿using System;
using System.Collections.Generic;

namespace FizzBuzz
{
    public class FizzBuzzPrinter
    {
        private readonly List<IFizzBuzzRule> _standardRules = new List<IFizzBuzzRule>();
        private readonly IFizzBuzzRule _defaultRule;
        private readonly IConsoleWriteLine _console;

        internal FizzBuzzPrinter(IEnumerable<IFizzBuzzRule> standardRules, IFizzBuzzRule defaultRule, IConsoleWriteLine console)
        {
            _standardRules.AddRange(standardRules);
            _defaultRule = defaultRule;
            _console = console;
        }

        /// <summary>
        /// Print fizzbuzz strings to the console for the passed in range.
        /// </summary>
        /// <param name="start">the start of the range</param>
        /// <param name="end">the end of the range</param>
        public void PrintFizzBuzzForRange(int start, int end)
        {
            for (long i = start; i <= end; i++)
            {
                _console.WriteLine(GetFizzBuzzStringForNumber((int)i));
            }
        }

        internal string GetFizzBuzzStringForNumber(int number)
        {
            string fizzBuzzString = String.Empty;
            for (int i = 0; i < _standardRules.Count; i++)
            {
                fizzBuzzString += _standardRules[i].GetStringForInt(number);
            }

            if (fizzBuzzString != string.Empty)
            {
                return fizzBuzzString;
            }
            return _defaultRule.GetStringForInt(number);
        }
    }
}