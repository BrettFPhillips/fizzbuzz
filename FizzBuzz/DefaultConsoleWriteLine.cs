using System;

namespace FizzBuzz
{
    /// <summary>
    /// Default implentation for console writeline, just pass through to the actual console.
    /// </summary>
    internal class DefaultConsoleWriteLine : IConsoleWriteLine
    {
        public void WriteLine(string line)
        {
            Console.WriteLine(line);
        }
    }
}