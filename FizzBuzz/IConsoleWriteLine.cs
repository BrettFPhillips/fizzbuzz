namespace FizzBuzz
{
    /// <summary>
    /// Wrapping the console writeline behavior with an interface to make testing easier
    /// </summary>
    internal interface IConsoleWriteLine
    {
        void WriteLine(string line);
    }
}