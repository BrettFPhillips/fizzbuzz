﻿using FizzBuzz;

namespace ConsoleApplication1
{
    internal class Program
    {
        private static void Main(string[] args)
        {
            FizzBuzzPrinterFactory.CustomFizzBuzzPrinter
                (   new[]
                    {
                        FizzBuzzRuleFactory.BuildDivisibleRule(3,"fizz"),
                        FizzBuzzRuleFactory.BuildDivisibleRule(5,"buzz"),
                        FizzBuzzRuleFactory.BuildDivisibleRule(10,"darp") // Just for you Alonso
                    },
                    new NumberToStringRule()
                ).PrintFizzBuzzForRange(0, 100);
        }
    }
}