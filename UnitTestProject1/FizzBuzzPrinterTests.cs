﻿using FizzBuzz;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace UnitTestProject1
{
    [TestClass]
    public class FizzBuzzPrinterTests
    {
        [TestMethod]
        public void MultipleRulesAccumulateTest()
        {
            var fizzBuzzRuleSet = new[] { FizzBuzzRuleFactory.BuildDivisibleRule(1, "first"), FizzBuzzRuleFactory.BuildDivisibleRule(1, "second") };
            Assert.AreEqual("firstsecond", FizzBuzzPrinterFactory.CustomFizzBuzzPrinter(fizzBuzzRuleSet, new NumberToStringRule()).GetFizzBuzzStringForNumber(2133));
        }

        [TestMethod]
        public void DefaultRuleUsedTest()
        {
            var printer = FizzBuzzPrinterFactory.CustomFizzBuzzPrinter(
                new[]
                    {
                        FizzBuzzRuleFactory.BuildDivisibleRule(5,"buzz")
                    },
                new RelayFizzBuzzRule(_=>"testString"));
            Assert.AreEqual("testString", printer.GetFizzBuzzStringForNumber(7));
        }

        private class ConsoleWritelineCounter : IConsoleWriteLine
        {
            public long LongCount { get; private set; }

            public void WriteLine(string line)
            {
                LongCount++;
            }
        }

        /// <summary>
        /// TODO: this test is in response to a bug, but it takes nearly a minute to run.
        /// Need to find a way to test the same functionality without it taking so long no one will run the test
        /// </summary>
        [TestMethod, Ignore]
        public void FullRangeTest()
        {
            var writeLineCounter = new ConsoleWritelineCounter();

            var printer = new FizzBuzzPrinter(
                new IFizzBuzzRule[] { },
                new RelayFizzBuzzRule(_ => string.Empty),
                writeLineCounter);

            printer.PrintFizzBuzzForRange(int.MinValue, int.MaxValue);

            Assert.AreEqual((long)int.MaxValue - (long)int.MinValue + 1L, writeLineCounter.LongCount);
        }

        [TestMethod]
        public void BasicRangeTest()
        {
            var writeLineCounter = new ConsoleWritelineCounter();

            var printer = new FizzBuzzPrinter(
                new IFizzBuzzRule[] { },
                new RelayFizzBuzzRule(_ => string.Empty),
                writeLineCounter);

            printer.PrintFizzBuzzForRange(-100, 100);

            Assert.AreEqual(201, writeLineCounter.LongCount);
        }
    }
}