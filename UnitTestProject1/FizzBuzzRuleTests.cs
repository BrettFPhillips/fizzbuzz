﻿using FizzBuzz;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace UnitTestProject1
{
    [TestClass]
    public class FizzBuzzRuleTests
    {
        [TestMethod]
        public void DivisibleRuleIsDivisibleTest()
        {
            Assert.AreEqual("testString", FizzBuzzRuleFactory.BuildDivisibleRule(4, "testString").GetStringForInt(8));
        }

        [TestMethod]
        public void DivisibleRuleIsNotDivisibleTest()
        {
            Assert.AreEqual(string.Empty, FizzBuzzRuleFactory.BuildDivisibleRule(4, "testString").GetStringForInt(7));
        }

        [TestMethod]
        public void DefaultRuleTest()
        {
            Assert.AreEqual("-8", new NumberToStringRule().GetStringForInt(-8));
        }
    }
}